/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab7;

/**
 *
 * @author HP
 */
public class Person {
    String name;
    double weight;
    double height;
    Weight unitWeight;
    Height unitHeight;
    
       public Person (String name, double weight,Weight unitWeight, double height,  Height unitHeight){
           this.name= name;
           this.weight=weight;
           this.height= height;
           this.unitHeight=unitHeight;
           this.unitWeight=unitWeight;
}
    public String getName(){
        return name;
    }
    
    public void setName(String name){
        this.name=name;
    }
    public double getWeight(){
        return weight;
    }
    public void setWeight(double weight){
        this.weight=weight;
    }
    
    public double getHeight(){
        return height;
    }
    public void setHeight(double height){
        this.height=height;
    }
    public double getBMI(){
        double weightInKilos=0;
        double heightInMetres=0;
    switch (unitWeight){
        case K : weightInKilos=weight;
        case LB : weightInKilos = weight*0.4535;
        break;
    }
      switch(unitHeight){
    case  M : heightInMetres=height;
    case IN : heightInMetres=height*0.0254;
    break;
}
          
            return weight / (Math.pow(height ,2));
    }
    }

        
    
    
